package com.industry62.decathlon.model;

public enum Unit {

  CENTIMETER, METER, SECOND

}
