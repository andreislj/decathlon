package com.industry62.decathlon.model;

import com.industry62.decathlon.util.CompareUtil;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class TrackEventPointsStrategy extends EventPointsStrategy<TrackEvent> {

  public TrackEventPointsStrategy(final TrackEvent event, final BigDecimal a, final BigDecimal b, final BigDecimal c) {
    super(event, a, b, c);
  }

  @Override
  public BigDecimal getPoints(final BigDecimal time) {
    //The quantity B gives the cut-off time at and above which you will score zero points.
    //T is always less than B in practice -- unless someone falls over and crawls to the finish!
    if (CompareUtil.isXGreaterThanOrEqualsToY(time, b)) {
      return BigDecimal.ZERO;
    }

    final double pow = Math.pow(b.subtract(time).doubleValue(), c.doubleValue());
    //The points awarded (decimals are rounded to the nearest whole number to avoid fractional points) in each track event
    final BigDecimal points = a.multiply(new BigDecimal(pow)).setScale(0, RoundingMode.HALF_UP);
    return points;
  }
}
