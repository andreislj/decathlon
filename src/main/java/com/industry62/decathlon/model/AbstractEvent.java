package com.industry62.decathlon.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    property = "type")
@JsonSubTypes({
    @Type(value = FieldEvent.class, name = "fieldEvent"),
    @Type(value = TrackEvent.class, name = "trackEvent")
})
@RequiredArgsConstructor
public abstract class AbstractEvent {

  final private Integer id;
  final private String name;
  final private Unit resultUnit;

}
