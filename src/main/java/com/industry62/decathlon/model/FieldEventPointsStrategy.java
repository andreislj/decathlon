package com.industry62.decathlon.model;

import com.industry62.decathlon.util.CompareUtil;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class FieldEventPointsStrategy extends EventPointsStrategy<FieldEvent> {

  public FieldEventPointsStrategy(final FieldEvent event, final BigDecimal a, final BigDecimal b, final BigDecimal c) {
    super(event, a, b, c);
  }

  @Override
  public BigDecimal getPoints(final BigDecimal distance) {

    //You score zero points for a distance (result) equal to or less than B.
    if (CompareUtil.isXLessThanOrEqualsToY(distance, b)) {
      return BigDecimal.ZERO;
    }

    final double pow = Math.pow(distance.doubleValue() - b.doubleValue(), c.doubleValue());
    final BigDecimal points = a.multiply(new BigDecimal(pow)).setScale(0, RoundingMode.HALF_UP);
    return points;

  }
}
