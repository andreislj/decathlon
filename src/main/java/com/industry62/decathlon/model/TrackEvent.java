package com.industry62.decathlon.model;

//100 m, 400 m, 110 m hurdles, 1500 m
public class TrackEvent extends AbstractEvent {

  public TrackEvent(final Integer id, final String name, final Unit unit) {
    super(id, name, unit);
  }
}
