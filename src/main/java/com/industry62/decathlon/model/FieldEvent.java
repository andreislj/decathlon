package com.industry62.decathlon.model;


//Long jump, Shot put, High jump, Discus throw, Pole vault, Javelin throw
public class FieldEvent extends AbstractEvent {

  public FieldEvent(final Integer id, final String name, final Unit unit) {
    super(id, name, unit);
  }
}
