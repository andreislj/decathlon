package com.industry62.decathlon.model;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public abstract class EventPointsStrategy<T extends AbstractEvent> {

  final private T event;
  final protected BigDecimal a;
  final protected BigDecimal b;
  final protected BigDecimal c;

  public abstract BigDecimal getPoints(final BigDecimal result);

}
