package com.industry62.decathlon.util;

import java.math.BigDecimal;

public class CompareUtil {

  public static boolean isXGreaterThanOrEqualsToY(BigDecimal x, BigDecimal y) {
    return x.compareTo(y) >= 0;
  }

  public static boolean isXLessThanOrEqualsToY(final BigDecimal x, final BigDecimal y) {
    return x.compareTo(y) < 1;
  }

}
