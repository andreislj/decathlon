package com.industry62.decathlon.repository;

import com.industry62.decathlon.model.AbstractEvent;
import com.industry62.decathlon.model.EventPointsStrategy;
import com.industry62.decathlon.model.FieldEvent;
import com.industry62.decathlon.model.FieldEventPointsStrategy;
import com.industry62.decathlon.model.TrackEvent;
import com.industry62.decathlon.model.TrackEventPointsStrategy;
import com.industry62.decathlon.model.Unit;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class InMemoryEventsRepository {

  private final Map<Integer, EventPointsStrategy<? extends AbstractEvent>> eventStrategies;

  public InMemoryEventsRepository() {

    final List<EventPointsStrategy<? extends AbstractEvent>> strategiesList = List.of(
        new TrackEventPointsStrategy(new TrackEvent(1, "100 meter dash", Unit.SECOND), new BigDecimal("25.4347"), new BigDecimal("18"), new BigDecimal("1.81")),
        new FieldEventPointsStrategy(new FieldEvent(2, "Long Jump", Unit.CENTIMETER), new BigDecimal("0.14354"), new BigDecimal("220"), new BigDecimal("1.4")),
        new FieldEventPointsStrategy(new FieldEvent(3, "Shot Put", Unit.METER), new BigDecimal("51.39"), new BigDecimal("1.5"), new BigDecimal("1.05")),
        new FieldEventPointsStrategy(new FieldEvent(4, "High Jump", Unit.CENTIMETER), new BigDecimal("0.8465"), new BigDecimal("75"), new BigDecimal("1.42")),
        new TrackEventPointsStrategy(new TrackEvent(5, "400 meter run", Unit.SECOND), new BigDecimal("1.53775"), new BigDecimal("82"), new BigDecimal("1.81")),
        new TrackEventPointsStrategy(new TrackEvent(6, "110 meter hurdles", Unit.SECOND), new BigDecimal("5.74352"), new BigDecimal("28.5"), new BigDecimal("1.92")),
        new FieldEventPointsStrategy(new FieldEvent(7, "Discus Throw", Unit.METER), new BigDecimal("12.91"), new BigDecimal("4"), new BigDecimal("1.1")),
        new FieldEventPointsStrategy(new FieldEvent(8, "Pole Vault", Unit.CENTIMETER), new BigDecimal("0.2797"), new BigDecimal("100"), new BigDecimal("1.35")),
        new FieldEventPointsStrategy(new FieldEvent(9, "Javelin Throw", Unit.METER), new BigDecimal("10.14"), new BigDecimal("7"), new BigDecimal("1.08")),
        new TrackEventPointsStrategy(new TrackEvent(10, "1500 meter run", Unit.SECOND), new BigDecimal("0.03768"), new BigDecimal("480"), new BigDecimal("1.85"))
    );

    this.eventStrategies = strategiesList
        .stream()
        .collect(Collectors.toMap(strategy -> strategy.getEvent().getId(), s -> s, (a, b) -> b, TreeMap::new));

  }

  public EventPointsStrategy<? extends AbstractEvent> findById(final Integer eventId) {

    return Optional
        .ofNullable(eventStrategies.get(eventId))
        .orElseThrow(() -> new IllegalArgumentException(String.format("Event strategy by event id: %s does not exist.", eventId)));

  }

  public List<AbstractEvent> findAll() {
    return eventStrategies.values().stream().map(EventPointsStrategy::getEvent).collect(Collectors.toList());
  }

}
