package com.industry62.decathlon.service;

import com.industry62.decathlon.model.AbstractEvent;
import com.industry62.decathlon.model.EventPointsStrategy;
import com.industry62.decathlon.repository.InMemoryEventsRepository;
import java.math.BigDecimal;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

@Service
public class InMemoryPointsService implements PointsService {

  @Inject
  private InMemoryEventsRepository eventsRepository;

  public BigDecimal getPoints(final Integer eventId, final BigDecimal result) {
    final EventPointsStrategy<? extends AbstractEvent> eventStrategy = eventsRepository.findById(eventId);
    return eventStrategy.getPoints(result);
  }

}
