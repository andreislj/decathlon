package com.industry62.decathlon.service;

import java.math.BigDecimal;

public interface PointsService {

  BigDecimal getPoints(final Integer eventId, final BigDecimal result);

}
