package com.industry62.decathlon.service;

import com.industry62.decathlon.model.AbstractEvent;
import com.industry62.decathlon.repository.InMemoryEventsRepository;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

@Service
public class EventsService {

  @Inject
  private InMemoryEventsRepository eventsRepository;

  public List<AbstractEvent> getEvents() {
    return eventsRepository.findAll();
  }
}
