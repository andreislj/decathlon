package com.industry62.decathlon.rest;

import com.industry62.decathlon.rest.model.PointsResponse;
import com.industry62.decathlon.service.PointsService;
import java.math.BigDecimal;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//TODO (08.07.2021) Andrei Sljusar: @ExceptionHandler(RuntimeException.class)
@CrossOrigin
@RestController
@Slf4j
class PointsController {

  @Inject
  private PointsService pointsService;

  @GetMapping("/points")
  PointsResponse getPoints(@RequestParam Integer eventId, @RequestParam String result) {
    try {
      final BigDecimal points = pointsService.getPoints(eventId, new BigDecimal(result));
      return new PointsResponse(eventId, points.intValue(), "");
    } catch (final Exception e) {
      //TODO (11.07.2021) Andrei Sljusar: create Validator and throw ValidateException
      //TODO (11.07.2021) Andrei Sljusar: send error code to client?
      log.error(String.format("Error on getting points for event id: %s and result: %s", eventId, result), e);
      return new PointsResponse(eventId, null, "Format is wrong. Enter decimal number, e.g.: 12.34");
    }

  }

}
