package com.industry62.decathlon.rest.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PointsResponse {

  private Integer eventId;
  private Integer points;
  private String errorMsg;

}
