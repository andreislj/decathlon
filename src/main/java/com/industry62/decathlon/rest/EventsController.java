package com.industry62.decathlon.rest;

import com.industry62.decathlon.model.AbstractEvent;
import com.industry62.decathlon.service.EventsService;
import java.util.List;
import javax.inject.Inject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
class EventsController {

  @Inject
  private EventsService eventsService;

  @GetMapping("/events")
  List<AbstractEvent> getEvents() {
    return eventsService.getEvents();
  }

}
