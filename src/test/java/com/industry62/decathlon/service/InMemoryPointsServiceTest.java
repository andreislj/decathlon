package com.industry62.decathlon.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.industry62.decathlon.model.EventPointsStrategy;
import com.industry62.decathlon.model.TrackEvent;
import com.industry62.decathlon.model.TrackEventPointsStrategy;
import com.industry62.decathlon.model.Unit;
import com.industry62.decathlon.repository.InMemoryEventsRepository;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InMemoryPointsServiceTest {

  @InjectMocks
  private InMemoryPointsService pointsService;

  @Mock
  private InMemoryEventsRepository eventsRepository;

  @Test
  void getPoints() {

    final EventPointsStrategy strategy = new TrackEventPointsStrategy(new TrackEvent(1, "100 meter dash", Unit.METER), new BigDecimal("25.4347"), new BigDecimal("18"), new BigDecimal("1.81"));

    Mockito.when(eventsRepository.findById(1)).thenReturn(strategy);

    final BigDecimal actual = pointsService.getPoints(1, new BigDecimal("12.34"));

    assertEquals(new BigDecimal("586"), actual);

  }
}
