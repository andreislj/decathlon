package com.industry62.decathlon.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

class TrackEventPointsStrategyTest {

  private TrackEventPointsStrategy strategy = new TrackEventPointsStrategy(new TrackEvent(1, "100 meter dash", Unit.METER), new BigDecimal("25.4347"), new BigDecimal("18"), new BigDecimal("1.81"));

  @Test
  void getPoints() {

    assertEquals(new BigDecimal("2"), strategy.getPoints(new BigDecimal("17.75")));
    assertEquals(BigDecimal.ONE, strategy.getPoints(new BigDecimal("17.83")));
    assertEquals(BigDecimal.ZERO, strategy.getPoints(new BigDecimal("18")));

  }

}
