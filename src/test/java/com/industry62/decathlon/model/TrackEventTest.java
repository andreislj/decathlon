package com.industry62.decathlon.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class TrackEventTest {

  @Test
  void getUnit() {

    assertEquals(Unit.SECOND, new TrackEvent(1, "any", Unit.SECOND).getResultUnit());

  }
}
