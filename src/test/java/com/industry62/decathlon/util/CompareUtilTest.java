package com.industry62.decathlon.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

class CompareUtilTest {

  @Test
  void isXGreaterThanOrEqualsToY() {

    assertTrue(CompareUtil.isXGreaterThanOrEqualsToY(new BigDecimal("1.01"), new BigDecimal("1")));
    assertTrue(CompareUtil.isXGreaterThanOrEqualsToY(new BigDecimal("1"), new BigDecimal("1")));
    assertFalse(CompareUtil.isXGreaterThanOrEqualsToY(new BigDecimal("0.99"), new BigDecimal("1")));

  }

  @Test
  void isXLessThanOrEqualsToY() {

    assertFalse(CompareUtil.isXLessThanOrEqualsToY(new BigDecimal("1.01"), new BigDecimal("1")));
    assertTrue(CompareUtil.isXLessThanOrEqualsToY(new BigDecimal("1"), new BigDecimal("1")));
    assertTrue(CompareUtil.isXLessThanOrEqualsToY(new BigDecimal("0.99"), new BigDecimal("1")));

  }
}
