package com.industry62.decathlon.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.industry62.decathlon.model.FieldEvent;
import com.industry62.decathlon.model.TrackEvent;
import com.industry62.decathlon.model.Unit;
import com.industry62.decathlon.service.EventsService;
import java.util.LinkedHashMap;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@ContextConfiguration(classes = {EventsController.class})
class EventsControllerTest extends AbstractControllerTest {

  @MockBean
  private EventsService eventsService;

  @Test
  void getEvents() throws Exception {

    Mockito.when(eventsService.getEvents()).thenReturn(List.of(
        new FieldEvent(1, "any 1", Unit.METER),
        new TrackEvent(2, "any 2", Unit.CENTIMETER)));

    final MvcResult result = mockMvc
        .perform(MockMvcRequestBuilders.get("/events")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();

    final String content = result.getResponse().getContentAsString();

    final List<LinkedHashMap<String, Object>> events = objectMapper.readValue(content, List.class);
    assertEquals(2, events.size());

    assertEquals(1, events.get(0).get("id"));
    assertEquals(2, events.get(1).get("id"));

  }
}
