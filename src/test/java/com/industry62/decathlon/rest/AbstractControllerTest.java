package com.industry62.decathlon.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.inject.Inject;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@WebMvcTest
abstract class AbstractControllerTest {

  @Inject
  protected MockMvc mockMvc;

  @Inject
  protected ObjectMapper objectMapper;

}
