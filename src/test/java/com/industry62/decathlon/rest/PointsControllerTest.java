package com.industry62.decathlon.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.industry62.decathlon.rest.model.PointsResponse;
import com.industry62.decathlon.service.InMemoryPointsService;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MvcResult;

@ContextConfiguration(classes = {PointsController.class})
class PointsControllerTest extends AbstractControllerTest {

  @MockBean
  private InMemoryPointsService pointsService;

  @Test
  void getPoints() throws Exception {

    Mockito.when(pointsService.getPoints(1, new BigDecimal("123"))).thenReturn(new BigDecimal("321"));

    final MvcResult result = mockMvc
        .perform(get("/points").param("eventId", "1").param("result", "123")
            .contentType(APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    final String content = result.getResponse().getContentAsString();

    final PointsResponse pointsResponse = objectMapper.readValue(content, PointsResponse.class);
    assertEquals(1, pointsResponse.getEventId());
    assertEquals(321, pointsResponse.getPoints());
    assertEquals("", pointsResponse.getErrorMsg());

  }

  @Test
  void getPointsWrongFormat() throws Exception {

    final MvcResult result = mockMvc
        .perform(get("/points").param("eventId", "2").param("result", "abc")
            .contentType(APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    final String content = result.getResponse().getContentAsString();

    final PointsResponse pointsResponse = objectMapper.readValue(content, PointsResponse.class);
    assertEquals(2, pointsResponse.getEventId());
    assertNull(pointsResponse.getPoints());
    assertEquals("Format is wrong. Enter decimal number, e.g.: 12.34", pointsResponse.getErrorMsg());

  }
}
