package com.industry62.decathlon.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.industry62.decathlon.model.AbstractEvent;
import com.industry62.decathlon.model.EventPointsStrategy;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

class InMemoryEventsRepositoryTest {

  private final InMemoryEventsRepository eventsRepository = new InMemoryEventsRepository();

  @Test
  void findById_Found() {

    final EventPointsStrategy<? extends AbstractEvent> actual = eventsRepository.findById(1);

    assertEquals("100 meter dash", actual.getEvent().getName());

  }

  @Test
  void findById_NotFound() {

    final IllegalArgumentException actualException = assertThrows(IllegalArgumentException.class, () -> {
      eventsRepository.findById(11);
    });

    assertEquals("Event strategy by event id: 11 does not exist.", actualException.getMessage());

  }

  @Test
  void findAll() {

    final List<AbstractEvent> events = eventsRepository.findAll();

    final List<Integer> expected = IntStream.range(1, 11).boxed().collect(Collectors.toList());
    final List<Integer> actual = events.stream().map(e -> e.getId()).collect(Collectors.toList());
    assertEquals(expected, actual);

  }

}
