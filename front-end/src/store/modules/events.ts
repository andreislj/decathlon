import {getModule, Module, MutationAction, VuexModule} from 'vuex-module-decorators'

import store from '@/store'
import {Event} from '@/store/models'
import {getEvents} from '@/store/api';

@Module({
      namespaced: true,
      name: 'events',
      store,
      dynamic: true,
    },
)
class EventsModule extends VuexModule {

  public events: Event[] = [];

  get _events(): Event[] {
    return this.events;
  }

  @MutationAction({mutate: ['events']})
  public async getEvents() {
    const events: Event[] | undefined = await getEvents()
    return {events}
  }

}

export default getModule(EventsModule);
