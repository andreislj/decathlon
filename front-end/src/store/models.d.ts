export interface Event {
  id: number,
  type: string,
  name: string,
  resultUnit: string,
}

export interface PointsResponse {
  eventId: number;
  points: number;
  errorMsg: errorMsg;
}

export interface PointsRequest {
  eventId: number;
  result: string;
}
