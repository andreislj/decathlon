import axios from 'axios'
import {Event, PointsRequest, PointsResponse} from '@/store/models';

export const myApi = axios.create({
  baseURL: process.env.VUE_APP_REST_API_baseURL,
  withCredentials: false
})

export async function getEvents(): Promise<Event[] | undefined> {
  try {
    const response = await myApi.get('events');
    return (response.data as Event[]);
  } catch (e) {
    return Promise.reject(e);
  }
}

export async function getPoints(pointsRequest: PointsRequest): Promise<PointsResponse | undefined> {
  try {
    const response = await myApi.get('points', {params: {eventId: pointsRequest.eventId, result: pointsRequest.result}});
    return (response.data as PointsResponse);
  } catch (e) {
    return Promise.reject(e);
  }
}
