//https://cli.vuejs.org/config/#filenamehashing
module.exports = {
  pages: {
    index: {
      entry: 'src/main.ts',
      title: 'Industry62: Decathlon scoring points'
    }
  },
  transpileDependencies: ["vuetify"],
  devServer: {
    publicPath: '/',
    hot: true,
    open: true,
    historyApiFallback: true,
  }
};
