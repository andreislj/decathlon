## Clone a repository

git clone git@bitbucket.org:andreislj/decathlon.git

---

## How to run application

1. java version: openjdk 14.0.2 
2. Run on port: 8080: ./gradlew bootRun. Open: http://localhost:8080/.

---

## done based on:

1. https://nrich.maths.org/8346
2. http://www.ten7events.com/m10pts/

---

## How to run tests

1. ./gradlew test --rerun-tasks

---

## Package front-end inside server

1. cd front-end
2. npm install   
3. npm run build-prod
4. cd ..
5. ./gradlew copyFrontendDist

---

## TODO

1. Result validation: min-max
2. Improve showing errors on ui. Now ui is jumping.
3. time and distance can be Quantity (number and unit). Now BigDecimal.
4. Separate event and result units
5. Global exception handler: @ExceptionHandler
6. Acceptance tests.
---
